from yamello import serialize, deserialize
from yamello.yamello_token import Parser, Token


def main():
    ''' deserialization '''
    with open('dev_resources/dev.in.yaml', mode = 'r') as in_stream:
        in_str = in_stream.read()

    pp = Parser(serial = in_str)
    pp.tokenize()
    pp.parse()

    # for entry in pp.token:
    #     token: Token = entry.value
    #     endstr: str = ' '  # reset
    #     if token.ttype == Token.TType.LINEBREAK: endstr = '\n'
    #     print(token, end = endstr)

    print(pp.transpile())

    with open('../demo/demo_resources/demo.in.yaml', mode = 'r') as in_stream_demo:
        in_str_demo = in_stream_demo.read()
    print(deserialize(serial = in_str_demo))



    # ''' operations on the data '''
    # out_data = in_data
    #
    # ''' serialization '''
    # out_str: str = serialize(out_data)
    # with open('demo_resources/demo.out.yaml', mode='w') as out_stream:
    #     out_stream.write(out_str)


if __name__ == '__main__': main()
