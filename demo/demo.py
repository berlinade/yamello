from yamello import serialize, deserialize


def demo():
    ''' deserialization '''
    with open('demo_resources/demo.in.yaml', mode = 'r') as in_stream:
        in_str = in_stream.read()
    in_data = deserialize(in_str, vec_constructor = tuple)
    print(in_data)

    ''' operations on the data '''
    out_data = in_data

    ''' serialization '''
    out_str: str = serialize(out_data)
    with open('demo_resources/demo.out.yaml', mode = 'w') as out_stream:
        out_stream.write(out_str)


def demo2():
    ''' deserialization '''
    with open('demo_resources/demo.in.2.yaml', mode = 'r') as in_stream:
        in_str = in_stream.read()
    in_data = deserialize(in_str, vec_constructor = tuple)
    print(in_data)

    ''' operations on the data '''
    out_data = in_data

    ''' serialization '''
    out_str: str = serialize(out_data)
    with open('demo_resources/demo.out.2.yaml', mode = 'w') as out_stream:
        out_stream.write(out_str)


def main():
    demo()
    demo2()


if __name__ == '__main__': main()
